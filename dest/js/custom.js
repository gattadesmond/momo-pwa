var swipeToClosePopup = app.popup.create({
  el: ".cashback-popup-swipe-handler",
  swipeToClose: "to-bottom"
});

$("input[type=radio][name=cashback-media-radio]").change(function() {
  app.popup.close();
});

function cashbackSearch() {
  // Dummy items array
  var items = [];
  for (var i = 1; i <= 100; i++) {
    items.push({
      title: "Đối tác " + i + "momo",
      subtitle: "Hoàn tiền " + i
    });
  }

  var virtualList = app.virtualList.create({
    // List Element
    el: ".virtual-list",
    // Pass array with items
    items: items,
    // Custom search function for searchbar
    searchAll: function(query, items) {
      var found = [];
      for (var i = 0; i < items.length; i++) {
        if (
          items[i].title.toLowerCase().indexOf(query.toLowerCase()) >= 0 ||
          query.trim() === ""
        )
          found.push(i);
      }
      return found; //return array with mathced indexes
    },
    // List item Template7 template
    itemTemplate:
      '<div class="cashback-hori-item">' +
      '	<a href="/momo-single/" class="link-absolute"></a>' +
      '	<div class="cashback-hori-img">' +
      '		<div class="embed-responsive embed-responsive-1by1">' +
      '			<img src="https://img.mservice.io/momo_app_v2/new_version/img/icon/Cashback_BaskinRobins.png"' +
      '				class="embed-responsive-img " alt="">' +
      "		</div>" +
      "	</div>" +
      '	<div class="cashback-hori-content">' +
      '		<div class="name text-truncate">{{title}}</div>' +
      '		<div class="desc text-truncate">{{subtitle}}</div>' +
      "	</div>" +
      '	<div class="cashback-hori-value">' +
      '		<div class="cashback-hori-number flex-center text-center">' +
      "			<div>" +
      "				Hoàn" +
      '				<span class="d-block font-weight-bold">47%</span>' +
      "			</div>" +
      "		</div>" +
      "	</div>" +
      "</div>",
    // Item height
    height: 73
  });
}
cashbackSearch();